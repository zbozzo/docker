{
  "name": "Docker",
  "description": "Build, check and inspect your containers with [Docker](https://www.docker.com/)",
  "template_path": "templates/gitlab-ci-docker.yml",
  "kind": "package",
  "variables": [
    {
      "name": "DOCKER_BUILD_TOOL",
      "type": "enum",
      "values": ["kaniko", "buildah", "dind"],
      "description": "The build tool to use for building container image",
      "default": "kaniko"
    },
    {
      "name": "DOCKER_KANIKO_IMAGE",
      "description": "The image used to run kaniko\n\n_for kaniko build only_",
      "default": "gcr.io/kaniko-project/executor:debug"
    },
    {
      "name": "DOCKER_BUILDAH_IMAGE",
      "description": "The image used to run buildah\n\n_for buildah build only_",
      "default": "quay.io/buildah/stable:latest"
    },
    {
      "name": "DOCKER_IMAGE",
      "description": "The image used to run the docker client\n\n_for Docker-in-Docker(dind) build only_",
      "default": "registry.hub.docker.com/library/docker:latest"
    },
    {
      "name": "DOCKER_DIND_IMAGE",
      "description": "The image used to run the Docker daemon\n\n_for Docker-in-Docker(dind) build only_",
      "default": "registry.hub.docker.com/library/docker:dind"
    },
    {
      "name": "DOCKER_SKOPEO_IMAGE",
      "description": "The image used to publish docker image with Skopeo",
      "default": "quay.io/skopeo/stable:latest"
    },
    {
      "name": "DOCKER_FILE",
      "description": "The path to your `Dockerfile`",
      "default": "$CI_PROJECT_DIR/Dockerfile"
    },
    {
      "name": "DOCKER_CONTEXT_PATH",
      "description": "The Docker [context path](https://docs.docker.com/engine/reference/commandline/build/#build-with-path) (working directory) - _only set if you want a context path different from the Dockerfile location_",
      "advanced": true
    },
    {
      "name": "DOCKER_CONFIG_FILE",
      "description": "Path to the [Docker configuration file](https://docs.docker.com/engine/reference/commandline/cli/#sample-configuration-file) (JSON)",
      "default": ".docker/config.json",
      "advanced": true
    },
    {
      "name": "DOCKER_SNAPSHOT_IMAGE",
      "description": "Docker snapshot image",
      "default": "$CI_REGISTRY_IMAGE/snapshot:$CI_COMMIT_REF_SLUG"
    },
    {
      "name": "DOCKER_RELEASE_IMAGE",
      "description": "Docker release image",
      "default": "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    },
    {
      "name": "DOCKER_BUILD_ARGS",
      "description": "Additional docker/kaniko/buildah build arguments"
    },
    {
      "name": "DOCKER_METADATA",
      "description": "Additional metadata to set as labels",
      "default": "--label org.opencontainers.image.url=${CI_PROJECT_URL} --label org.opencontainers.image.source=${CI_PROJECT_URL} --label org.opencontainers.image.title=${CI_PROJECT_PATH} --label org.opencontainers.image.ref.name=${CI_COMMIT_REF_NAME} --label org.opencontainers.image.revision=${CI_COMMIT_SHA} --label org.opencontainers.image.created=${CI_JOB_STARTED_AT}",
      "advanced": true
    },
    {
      "name": "DOCKER_PUBLISH_ARGS",
      "description": "Additional [`skopeo copy` arguments](https://github.com/containers/skopeo/blob/master/docs/skopeo-copy.1.md#options)"
    },
    {
      "name": "DOCKER_PROD_PUBLISH_STRATEGY",
      "description": "Defines the publish to production strategy.",
      "type": "enum",
      "values": ["none", "manual", "auto"],
      "default": "manual"
    },
    {
      "name": "DOCKER_SEMREL_RELEASE_DISABLED",
      "description": "Disable integration with the [semantic release template](https://gitlab.com/to-be-continuous/semantic-release/)",
      "type": "boolean"
    },
    {
      "name": "DOCKER_REGISTRY_MIRROR",
      "description": "URL of a Docker registry mirror to use instead of default `https://index.docker.io`\n\n_Used by `kaniko` and `dind` builds only_"
    },
    {
      "name": "CONTAINER_REGISTRIES_CONFIG_FILE",
      "description": "The [registries.conf](https://www.redhat.com/sysadmin/manage-container-registries) configuration to be used\n\n_Used by the `buildah` build only_",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "dockerfile-lint",
      "description": "This job performs a [Lint](https://github.com/projectatomic/dockerfile_lint) on your `Dockerfile`.",
      "enable_with": "DOCKER_LINT_ENABLED",
      "variables": [
        {
          "name": "DOCKER_LINT_IMAGE",
          "description": "The docker image to lint your Dockerfile",
          "default": "registry.hub.docker.com/projectatomic/dockerfile-lint:latest"
        },
        {
          "name": "DOCKER_LINT_ARGS",
          "description": "Additional `dockerfile_lint` arguments",
          "advanced": true
        }
      ]
    },
    {
      "id": "hadolint",
      "name": "Hadolint",
      "description": "This job performs a [Lint](https://github.com/hadolint/hadolint) on your `Dockerfile`.",
      "disable_with": "DOCKER_HADOLINT_DISABLED",
      "variables": [
        {
          "name": "DOCKER_HADOLINT_IMAGE",
          "description": "The docker image to lint your Dockerfile with Hadolint",
          "default": "registry.hub.docker.com/hadolint/hadolint:latest-alpine"
        },
        {
          "name": "DOCKER_HADOLINT_ARGS",
          "description": "Additional `hadolint` arguments",
          "advanced": true
        }
      ]
    },
    {
      "id": "healthcheck",
      "name": "Health Check",
      "description": "[Health Check](https://docs.docker.com/engine/reference/builder/#healthcheck) image analysis",
      "disable_with": "DOCKER_HEALTHCHECK_DISABLED",
      "variables": [
        {
          "name": "DOCKER_HEALTHCHECK_TIMEOUT",
          "type": "number",
          "description": "When testing an image, how long (in seconds) wait for the HealthCheck status",
          "default": "60",
          "advanced": true
        },
        {
          "name": "DOCKER_HEALTHCHECK_OPTIONS",
          "description": "Docker options for health check such as port mapping, environment...",
          "advanced": true
        },
        {
          "name": "DOCKER_HEALTHCHECK_CONTAINER_ARGS",
          "description": "Arguments sent to the running container for health check",
          "advanced": true
        }
      ]
    },
    {
      "id": "trivy",
      "name": "Trivy",
      "description": "[Trivy](https://github.com/aquasecurity/trivy) vulnerability analysis",
      "disable_with": "DOCKER_TRIVY_DISABLED",
      "variables": [
        {
          "name": "DOCKER_TRIVY_IMAGE",
          "description": "The docker image used to scan images with Trivy",
          "default": "registry.hub.docker.com/aquasec/trivy:latest",
          "advanced": true
        },
        {
          "name": "DOCKER_TRIVY_ADDR",
          "type": "url",
          "description": "The Trivy server address"
        },
        {
          "name": "DOCKER_TRIVY_SECURITY_LEVEL_THRESHOLD",
          "type": "enum",
          "values": ["UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL", "LOW,MEDIUM,HIGH,CRITICAL", "MEDIUM,HIGH,CRITICAL", "HIGH,CRITICAL", "CRITICAL"],
          "description": "Severities of vulnerabilities to be displayed (comma separated values: `UNKNOWN`, `LOW`, `MEDIUM`, `HIGH`, `CRITICAL`)",
          "default": "UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL"
        },
        {
          "name": "DOCKER_TRIVY_ARGS",
          "description": "Additional `trivy client` arguments",
          "default": "--ignore-unfixed --vuln-type os",
          "advanced": true
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [syft](https://github.com/anchore/syft)",
      "disable_with": "DOCKER_SBOM_DISABLED",
      "variables": [
        {
          "name": "DOCKER_SBOM_IMAGE",
          "default": "registry.hub.docker.com/anchore/syft:debug"
        },
        {
          "name": "DOCKER_SBOM_OPTS",
          "description": "Options for syft used for SBOM analysis",
          "default": "--catalogers rpm-db-cataloger,alpmdb-cataloger,apkdb-cataloger,dpkgdb-cataloger,portage-cataloger",
          "advanced": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-docker-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
